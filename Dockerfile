FROM alpine:latest

MAINTAINER Michael Hug

RUN apk add git automake autoconf build-base
RUN git clone https://git.savannah.nongnu.org/git/chktex.git
WORKDIR /chktex/chktex/
RUN ./autogen.sh
RUN ./configure
RUN make
